import { defineStore } from "pinia";
// 9e98e9b51ed1ec764e65da2a0c9f44be

export const instantWeather = defineStore('instant',()=>{

     
    async function getInstantWeather(lat,long){
        const response = await fetch(`https://api.openweathermap.org/data/2.5/weather?lat=${lat}&lon=${long}&lang=fr&appid=9e98e9b51ed1ec764e65da2a0c9f44be&units=metric`);
        const data = await response.json();
        return data
    }
        

    return {getInstantWeather}
});