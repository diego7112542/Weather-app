import { defineStore } from "pinia";
// 9e98e9b51ed1ec764e65da2a0c9f44be

export const hourlyWeather = defineStore('hourly',()=>{

     
    async function getHourlyWeather(lat,long){
        const response = await fetch(`https://api.openweathermap.org/data/2.5/forecast?lat=${lat}&lon=${long}&lang=fr&units=metric&appid=9e98e9b51ed1ec764e65da2a0c9f44be`);
        const data = await response.json();
        return data
    }
        

    return {getHourlyWeather}
});



