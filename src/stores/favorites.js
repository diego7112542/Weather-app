import { defineStore } from 'pinia';
import { ref } from 'vue';


export const formsHandler = defineStore('forms', () => {

    const favorites = ref(JSON.parse(localStorage.getItem('favorites')) || []);

    function addToFavorites(data){
        favorites.value.push(data);
        localStorage.setItem('favorites', JSON.stringify(favorites.value));
    }

    function deleteLocation(index){
        favorites.value.splice(index, 1);
        localStorage.setItem('favorites', JSON.stringify(favorites.value));
    }
    
    async function getCoord(location){
        const response = await fetch(`http://api.openweathermap.org/geo/1.0/direct?q=${location}&lang=fr&limit=1&appid=9e98e9b51ed1ec764e65da2a0c9f44be`)
        const data = await response.json();
        return data
    }
    return { getCoord, addToFavorites,deleteLocation, favorites };
});