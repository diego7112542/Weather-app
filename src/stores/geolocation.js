import { defineStore } from "pinia";


export const geolocate = defineStore('geolocate', () => {
    
    async function getPos() {
        return new Promise((resolve, reject) => {
            const success = (position) => {
                resolve({lat: position.coords.latitude, long: position.coords.longitude});
            };
            const error = () => {
                reject('Votre navigateur ne supporte pas la géolocalisation ou une erreur est survenue.');
            };
            navigator.geolocation.getCurrentPosition(success, error);
        });
    }

    return { getPos };
});